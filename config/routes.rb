Rails.application.routes.draw do

	#Define as rotas para a API com versionamento
  	namespace 'api' do
	    namespace 'v1' do
	      resources :users do
	      	collection do
			    post :register
			    post :login
			end
			member do
		      get :confirm_email
		    end
	      end
	      resources :relatives
	    end
	end

end
