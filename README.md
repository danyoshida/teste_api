# Teste API

## Endpoints

#### Usu�rios

- ** POST api/v1/users/register **
- ** POST api/v1/users/login **
- ** GET api/v1/users/:token/confirm_email **

#### Dependentes

- ** GET api/v1/relatives **
- ** GET api/v1/relatives/:id **
- ** POST api/v1/relatives **
- ** DELETE api/v1/relatives/:id **
- ** PUT api/v1/relatives **

##### Headers
- Content-Type: application/json
- Authorization: Bearer {token}


## Email

#### Configurando usu�rio e senha
Edite as vari�veis de ambiente em /config/application.yml:

- ** GMAIL_USER: exemplo@gmail.com **
- ** GMAIL_PASS: 123456 **

#### Rodando a aplica��o e executando processos background
- ** rails server & bundle exec rake jobs:work **







