class RenameTypeToKinship < ActiveRecord::Migration[5.2]
  def change
  	rename_column :relatives, :type, :kinship
  end
end
