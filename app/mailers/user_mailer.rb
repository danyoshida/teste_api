class UserMailer < ApplicationMailer
	default from: "Teste API <exemplo@gmail.com>"

  	def registration_confirmation(user)
   		@user = user
    	mail(to: @user.email, subject: 'Confirmação de cadastro')
  	end
end
