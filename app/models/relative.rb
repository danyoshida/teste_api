class Relative < ApplicationRecord
	
	belongs_to :user
 	
 	# Validações
	validates :name, presence: true
	validates :kinship, presence: true

end
