class User < ApplicationRecord
	
	before_create -> {self.token = generate_token}
	has_many :relatives

	# Validações
	validates_presence_of :name, :email, :cpf, :password_digest
   	validates :email, uniqueness: true
 
   	#encrypt password
   	has_secure_password

   	# Confirma o email de cadastro para ativação do usuário
	def email_activate
	    self.email_confirmed = true
		save!(:validate => false)
	end

   	private

   	# Gera um token único para o usuário cadastrado
	def generate_token
	    loop do
	      token = SecureRandom.hex
	      return token unless User.exists?({token: token})
	    end
	end

end
