module Api
    module V1
        class RelativesController < ApplicationController
        	include ActionController::HttpAuthentication::Token::ControllerMethods
        	before_action :authenticate
        	
        	# Token auth
        	def authenticate
			    authenticate_or_request_with_http_token do |token, options|
			      @user = User.find_by(token: token)
			    end
			end

			# Lista todos os dependentes
			def index

				# Busca todos os dependentes
				@relatives = Relative.order('created_at DESC');

				# Retorna o json com a lista de dependentes cadastrados
				render json: {success: "true", data: @relatives},status: :ok

			end

			# Consulta um dependente
			def show

				# Busca um dependente com o ID informado
				@relative = Relative.find(params[:id])

				# Retorna o json com os dados do dependente
				render json: {success: "true", data: @relative}, status: :ok

			end

			# Cadastra um dependente
			def create

				# Realiza uma tentativa de cadastro de um dependente
			    @relative = @user.relatives.new(relative_params)

			    # Se o dependente foi cadastrado
			    if @relative.save

			    	# Retorna o json com os dados do dependente
			        render json: {success: "true", data: @relative}, status: :created

			    # Se o dependente não foi cadastrado 
			    else

			    	# Retorna o json com os erros de cadastro
			        render json: {success: "false", errors: @relative.errors}, status: :unprocessable_entity

			    end
			end

			# Exclui um dependente
			def destroy

				# Excluí o dependente com o ID informado
				@relative = Relative.find(params[:id])
				@relative.destroy

				# Retorna os dados do dependente excluído
				render json: {success: "true"}, status: :ok

			end

			# Edita um dependente
			def update

				# Busca o dependente com ID informado
				@relative = Relative.find(params[:id])

				# Realiza a edição com os dados informados
				if @relative.update_attributes(relative_params)

					# Retorna o json com os dados dependente
					render json: {success: "true", data: @relative}, status: :ok
				
				# Se não foi possível atualizar os dados
				else

					# Retorna o json com os erros
					render json: {success: "false", errors: @relative.errors}, status: :unprocessable_entity

				end
			end

			def relative_params
				params.permit(
				  :name,
				  :kinship,
				  :user_id
				)
			end

        end
    end
end