module Api
    module V1
        class UsersController < ApplicationController

        	# Cadastra um usuário
			def register

				# Realiza a tentativa de cadastro do usuário
				@user = User.create(user_params)

				# Se o usuário foi cadastrado
				if @user.save
					
					# Adiciona um processo background para envio do email de confirmação após o cadastro
					SendEmailJob.set(wait: 20.seconds).perform_later(@user)

					# Retorna o json com os dados do usuário cadastrado
					render json: {success: "true", data: @user}, status: :created
				
				#Se o usuário não foi cadastrado
				else
					
					# Retorna o json com os erros de cadastro
					render json: {success: "false", errors: @user.errors}, status: :unprocessable_entity
				
				end 

			end

			# Realiza a autenticação e login do usuário
			def login

				# Realiza uma tentativa de login por email
				@user = User.find_by_email(params[:email])
				
				# Se o usuário existe e a senha enviada está correta
				if @user && @user.authenticate(params[:password])

					# Se o email de cadastro foi confirmado
					if @user.email_confirmed

						# Retorna o json com os dados do usuário logado
						render json: {success: "true", data: @user}, status: :ok

					# Se o email de cadastro não foi confirmado
					else

						# Retorna o json com a mensagem de erro
						render json: {success: 'false', error: 'Email não confirmado'}, status: :unauthorized

					end

				# Se o usuário é inválido
				else

					# Retorna o json com o status de falha
					render json: {success: 'false', error: 'Usuário inválido'}, status: :unauthorized

				end
			end

			# Confirma o cadastro a partir do email de confirmação com o token do usuário
			def confirm_email

				# Recupera os dados do usuário a partir do token
			    @user = User.find_by_token(params[:id])

			    # Se existe um usuário com token informado
			    if @user

			    	# Realiza a confirmação do email de cadastro
			      	@user.email_activate

			      	# Retorna o json com a mensagem de sucesso e os dados do usuário
			      	render json: {success: 'true', data: @user}, status: :ok 
			   	
			   	#Se não existe um usuário com o token informado
			    else
			    	
			    	# Retorna o json com o status de falha
			    	render json: {success: 'false'}, status: :unprocessable_entity
			   
			    end
			end

			private

			def user_params
				params.permit(
				  :name,
				  :email,
				  :cpf,
				  :password	
				)
			end

        end
    end
end